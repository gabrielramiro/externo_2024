package com.bicicletario.externo.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class Erro {
    @Getter
    @Setter
    private String codigo;

    @Getter @Setter
    private String mensagem;
}