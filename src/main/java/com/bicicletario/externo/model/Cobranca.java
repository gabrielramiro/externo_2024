package com.bicicletario.externo.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@NoArgsConstructor
public class Cobranca {
    @Getter @Setter
    private Integer id;

    @Getter @Setter
    private String status;

    @Getter @Setter
    private String horaSolicitacao;

    @Getter @Setter
    private String horaFinalizacao;

    @Getter @Setter
    private Double valor;

    @Getter @Setter
    private Integer ciclista;

}
