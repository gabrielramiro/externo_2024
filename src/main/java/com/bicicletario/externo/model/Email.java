package com.bicicletario.externo.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;
@Component
@NoArgsConstructor
public class Email {
    //    private static final AtomicInteger contador = new AtomicInteger(0);
    @Getter
    @Setter
    private Integer id;

    @Getter @Setter
    private String email;

    @Getter @Setter
    private String assunto;

    @Getter @Setter
    private String mensagem;

}