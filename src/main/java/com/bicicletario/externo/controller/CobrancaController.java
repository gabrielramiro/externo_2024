package com.bicicletario.externo.controller;


import com.bicicletario.externo.DTO.CobrancaDTO;
import com.bicicletario.externo.service.CobrancaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class CobrancaController {
    @Autowired
    CobrancaService cobrancaService;

    @PostMapping("/filaCobranca")
    public ResponseEntity<Object> realizaCobranca(@RequestBody CobrancaDTO cobrancaDTO){
        Map serviceResponse = cobrancaService.incluirCobrancaNaFila(cobrancaDTO.getValor(), cobrancaDTO.getCiclista());
        if (serviceResponse.get("codigo") == null){
            return ResponseEntity.status(200).body(serviceResponse);
        }
        return  ResponseEntity.status(Integer.parseInt(serviceResponse.get("codigo").toString())).body(serviceResponse);
    }

    @GetMapping("/cobranca/{idCobranca}")
    public ResponseEntity<Object> getCobranca(@PathVariable Integer idCobranca) {
        Map serviceResponse = cobrancaService.getCobranca(idCobranca);
        if (serviceResponse.get("codigo") == null){
            return ResponseEntity.status(200).body(serviceResponse);
        }
        return  ResponseEntity.status(Integer.parseInt(serviceResponse.get("codigo").toString())).body(serviceResponse);
    }
}
