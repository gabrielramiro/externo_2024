package com.bicicletario.externo.controller;

import com.bicicletario.externo.DTO.EmailDTO;
import com.bicicletario.externo.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class EmailController {
    @Autowired
    private EmailService emailService;

    @PostMapping ("/enviarEmail")
    public ResponseEntity<Object> enviaEmail(@RequestBody EmailDTO emailDTO) {
           Map serviceResponse =  emailService.enviaEmail(emailDTO.getEmail(), emailDTO.getAssunto(), emailDTO.getMensagem());
           //se não tiver código não deu erro
           if (serviceResponse.get("codigo") == null){
               return ResponseEntity.status(200).body(serviceResponse);
           }
           return  ResponseEntity.status(Integer.parseInt(serviceResponse.get("codigo").toString())).body(serviceResponse);
    }
}
