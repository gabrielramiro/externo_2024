package com.bicicletario.externo.DTO;

import lombok.Getter;
import lombok.Setter;

public class EmailDTO {
    @Getter@Setter
    private String email;

    @Getter@Setter
    private String assunto;

    @Getter@Setter
    private String mensagem;
}

