package com.bicicletario.externo.DTO;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Component
public class CobrancaDTO {
    @Getter @Setter
    private Double valor;
    @Getter @Setter
    private Integer ciclista;
}
