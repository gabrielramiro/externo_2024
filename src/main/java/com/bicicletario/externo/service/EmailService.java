package com.bicicletario.externo.service;

//import com.bicicletario.externo.exceptions.DadosInvalidosException;
//import com.bicicletario.externo.exceptions.DadoInexistenteException;
//import com.bicicletario.externo.model.Email;
//import com.bicicletario.externo.model.Erro;
//import com.bicicletario.externo.model.NovoEmail;
//import org.jetbrains.annotations.NotNull;
//import org.springframework.beans.factory.annotation.Autowired;
import com.bicicletario.externo.model.Email;
import com.bicicletario.externo.model.Erro;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.client.RestTemplate;


import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class EmailService {

    private static final AtomicInteger contador = new AtomicInteger(0);
    private Integer id;
    @Autowired
    Email email;
    @Autowired
    Erro erro;

    @Autowired
    JavaMailSender mailSender;

    private RestTemplate restTemplate;

    //dados para utiliozar a api de envio de email
    private static final String API_KEY = "f743d3bc9ea14e1bb4441dd87153d2ef";
    private static final String API_URL = "https://api.zerobounce.net/v2/validate?api_key=";

    public Map<String,Object> enviaEmail(String destinatario, String assunto, String mensagem) {
        ObjectMapper objectMapper = new ObjectMapper();

        String response = ValidaEmail(destinatario);
        Map<String, Object> responseMap = null;
        try {
            responseMap = objectMapper.readValue(response, Map.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if("valid".equalsIgnoreCase(responseMap.get("status").toString())) {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(destinatario);
            message.setSubject(assunto);
            message.setText(mensagem);
            message.setFrom("bicicletarioexterno@gmailcom");

            mailSender.send(message);

            this.email.setId(gerarID());
            this.email.setEmail(destinatario);
            this.email.setAssunto(assunto);
            this.email.setMensagem(mensagem);

            return objectMapper.convertValue(email, new TypeReference<Map<String,Object>>() {});
        }
        String mensagemErro = responseMap.get("sub_status").toString();
        if (mensagemErro.equalsIgnoreCase("failed_syntax_check")){
            erro.setCodigo("422");
            erro.setMensagem(mensagemErro);
        }
        else if (mensagemErro.equalsIgnoreCase("mailbox_not_found")) {
            erro.setCodigo("404");
            erro.setMensagem(mensagemErro);
        }
        else{
            erro.setCodigo("400");
            erro.setMensagem(mensagemErro);
        }
        return objectMapper.convertValue(erro, new TypeReference<Map<String,Object>>() {});
    }

    public String ValidaEmail(String email) {
        try {
            restTemplate = new RestTemplate();
            String url = API_URL + API_KEY + "&email=" + email;
            String response = restTemplate.getForObject(url, String.class);

            return response;
//            ObjectMapper objectMapper = new ObjectMapper();
//            JsonNode jsonNode = objectMapper.readTree(response);
//
//            String status = jsonNode.path("status").asText();
//            return "valid".equalsIgnoreCase(status);

        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }
    private Integer gerarID(){
        return this.id =  contador.incrementAndGet();
    }
}
