package com.bicicletario.externo.service;

import com.bicicletario.externo.model.Cobranca;
import com.bicicletario.externo.model.Erro;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicInteger;

import static com.bicicletario.externo.model.StatusCobranca.PENDENTE;

@Service
public class CobrancaService {
    private Integer id;
    private static final AtomicInteger contador = new AtomicInteger(0);

    @Getter @Setter
    private Queue<Cobranca> filaCobrancas = new LinkedList<>();

    @Autowired
    private Cobranca cobranca;
    @Autowired
    Erro erro;

    public Map<String, Object> incluirCobrancaNaFila(Double valor, Integer ciclista) {
        //valida dados
        ObjectMapper objectMapper = new ObjectMapper();
        if (isDadosCobrancaValidos(valor, ciclista)){
            this.cobranca = new Cobranca();
            this.cobranca.setId(gerarID());
            this.cobranca.setStatus(PENDENTE.name());
            this.cobranca.setHoraSolicitacao(formatarData(LocalDateTime.now()));
            this.cobranca.setHoraFinalizacao(formatarData(LocalDateTime.now()));
            this.cobranca.setValor(valor);
            this.cobranca.setCiclista(ciclista);
            filaCobrancas.add(cobranca);
            return objectMapper.convertValue(cobranca, new TypeReference<Map<String,Object>>() {});
        }
        else{
            erro.setCodigo("422");
            erro.setMensagem("Dados Inválidos");
            return  objectMapper.convertValue(erro, new TypeReference<Map<String,Object>>() {});
        }
    }

    public Map <String,Object> getCobranca(Integer id){
        ObjectMapper objectMapper = new ObjectMapper();

        for (Cobranca cobranca: filaCobrancas){
            if (cobranca.getId() == id){
                return objectMapper.convertValue(cobranca, new TypeReference<Map<String,Object>>() {});
            }
        }
        erro.setCodigo("404");
        erro.setMensagem("Cobrança não encontrada");
        return objectMapper.convertValue(erro, new TypeReference<Map<String,Object>>() {});
    }


    private boolean isDadosCobrancaValidos(Double valor, Integer ciclista){
        return (valor > 0
                && !(valor.isNaN())
                && ciclista >= 0
                && ciclista != null
                && ciclista instanceof Integer);
    }
    private Integer gerarID(){
        return this.id =  contador.incrementAndGet();
    }
    private String formatarData(LocalDateTime data){
        // Adicionar informações de fuso horário (UTC)
        ZonedDateTime dataZonada = data.atZone(ZoneOffset.UTC);

        // Formatar a data e hora como string
        String formatoDesejado = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(formatoDesejado);
        String dataFormatada = dataZonada.format(formatter);
        return dataFormatada;
    }
}
