package com.bicicletario.externo.DTO;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class EmailDTOTest {

    @Test
    public void testGettersAndSetters() {
        // Criar uma instância da classe EmailDTO
        EmailDTO emailDTO = new EmailDTO();

        // Definir valores usando os setters
        emailDTO.setEmail("test@example.com");
        emailDTO.setAssunto("Teste");
        emailDTO.setMensagem("Esta é uma mensagem de teste");

        // Verificar os valores usando os getters
        assertEquals("test@example.com", emailDTO.getEmail());
        assertEquals("Teste", emailDTO.getAssunto());
        assertEquals("Esta é uma mensagem de teste", emailDTO.getMensagem());
    }

    @Test
    public void testConstructorWithArgs() {
        // Criar uma instância da classe EmailDTO usando o construtor com argumentos
        EmailDTO emailDTO = new EmailDTO();
        emailDTO.setEmail("another@example.com");
        emailDTO.setAssunto("Outro Teste");
        emailDTO.setMensagem("Outra mensagem de teste");

        // Verificar os valores dos atributos
        assertEquals("another@example.com", emailDTO.getEmail());
        assertEquals("Outro Teste", emailDTO.getAssunto());
        assertEquals("Outra mensagem de teste", emailDTO.getMensagem());
    }

    @Test
    public void testDefaultConstructor() {
        // Criar uma instância da classe EmailDTO usando o construtor padrão
        EmailDTO emailDTO = new EmailDTO();

        // Verificar que os valores iniciais são nulos
        assertNull(emailDTO.getEmail());
        assertNull(emailDTO.getAssunto());
        assertNull(emailDTO.getMensagem());
    }

    @Test
    public void testSetEmail() {
        // Criar uma instância da classe EmailDTO
        EmailDTO emailDTO = new EmailDTO();

        // Definir o email
        emailDTO.setEmail("set@example.com");

        // Verificar o email
        assertEquals("set@example.com", emailDTO.getEmail());
    }

    @Test
    public void testSetAssunto() {
        // Criar uma instância da classe EmailDTO
        EmailDTO emailDTO = new EmailDTO();

        // Definir o assunto
        emailDTO.setAssunto("Assunto Definido");

        // Verificar o assunto
        assertEquals("Assunto Definido", emailDTO.getAssunto());
    }

    @Test
    public void testSetMensagem() {
        // Criar uma instância da classe EmailDTO
        EmailDTO emailDTO = new EmailDTO();

        // Definir a mensagem
        emailDTO.setMensagem("Mensagem Definida");

        // Verificar a mensagem
        assertEquals("Mensagem Definida", emailDTO.getMensagem());
    }
}
