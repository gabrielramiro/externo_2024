package com.bicicletario.externo.DTO;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CobrancaDTOTest {

    @Test
    public void testGettersAndSetters() {
        // Criar uma instância da classe CobrancaDTO
        CobrancaDTO cobrancaDTO = new CobrancaDTO();

        // Definir valores usando os setters
        cobrancaDTO.setValor(100.50);
        cobrancaDTO.setCiclista(123);

        // Verificar os valores usando os getters
        assertEquals(100.50, cobrancaDTO.getValor());
        assertEquals(123, cobrancaDTO.getCiclista());
    }

    @Test
    public void testSetValor() {
        // Criar uma instância da classe CobrancaDTO
        CobrancaDTO cobrancaDTO = new CobrancaDTO();

        // Definir o valor
        cobrancaDTO.setValor(200.75);

        // Verificar o valor
        assertEquals(200.75, cobrancaDTO.getValor());
    }

    @Test
    public void testSetCiclista() {
        // Criar uma instância da classe CobrancaDTO
        CobrancaDTO cobrancaDTO = new CobrancaDTO();

        // Definir o ciclista
        cobrancaDTO.setCiclista(456);

        // Verificar o ciclista
        assertEquals(456, cobrancaDTO.getCiclista());
    }

    @Test
    public void testDefaultConstructor() {
        // Criar uma instância da classe CobrancaDTO usando o construtor padrão
        CobrancaDTO cobrancaDTO = new CobrancaDTO();

        // Verificar que os valores iniciais são nulos ou zero
        assertNull(cobrancaDTO.getValor());
        assertNull(cobrancaDTO.getCiclista());
    }
}

