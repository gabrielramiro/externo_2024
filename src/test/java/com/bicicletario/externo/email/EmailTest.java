package com.bicicletario.externo.email;

import com.bicicletario.externo.model.Email;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class EmailTest {

    @Test
    public void testGettersAndSetters() {
        // Criar uma instância da classe Email
        Email email = new Email();

        // Definir valores usando os setters
        email.setId(1);
        email.setEmail("test@example.com");
        email.setAssunto("Test Assunto");
        email.setMensagem("Test Mensagens");

        // Verificar os valores usando os getters
        assertEquals(1, email.getId());
        assertEquals("test@example.com", email.getEmail());
        assertEquals("Test Assunto", email.getAssunto());
        assertEquals("Test Mensagens", email.getMensagem());
    }

    @Test
    public void testDefaultConstructor() {
        // Criar uma instância da classe Email usando o construtor padrão
        Email email = new Email();

        // Verificar que os valores iniciais são nulos
        assertNull(email.getId());
        assertNull(email.getEmail());
        assertNull(email.getAssunto());
        assertNull(email.getMensagem());
    }

    @Test
    public void testSetId() {
        // Criar uma instância da classe Email
        Email email = new Email();

        // Definir o id
        email.setId(123);

        // Verificar o id
        assertEquals(123, email.getId());
    }

    @Test
    public void testSetEmail() {
        // Criar uma instância da classe Email
        Email email = new Email();

        // Definir o email
        email.setEmail("email@example.com");

        // Verificar o email
        assertEquals("email@example.com", email.getEmail());
    }

    @Test
    public void testSetAssunto() {
        // Criar uma instância da classe Email
        Email email = new Email();

        // Definir o assunto
        email.setAssunto("Novo Assunto");

        // Verificar o assunto
        assertEquals("Novo Assunto", email.getAssunto());
    }

    @Test
    public void testSetMensagem() {
        // Criar uma instância da classe Email
        Email email = new Email();

        // Definir a mensagem
        email.setMensagem("Esta é uma nova mensagem");

        // Verificar a mensagem
        assertEquals("Esta é uma nova mensagem", email.getMensagem());
    }
}
