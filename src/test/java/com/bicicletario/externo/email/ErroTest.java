package com.bicicletario.externo.email;

import com.bicicletario.externo.model.Erro;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ErroTest {

    @Test
    public void testGettersAndSetters() {
        // Criar uma instância da classe Erro
        Erro erro = new Erro();

        // Definir valores usando os setters
        erro.setCodigo("404");
        erro.setMensagem("Not Found");

        // Verificar os valores usando os getters
        assertEquals("404", erro.getCodigo());
        assertEquals("Not Found", erro.getMensagem());
    }

    @Test
    public void testConstructorWithArgs() {
        // Criar uma instância da classe Erro usando o construtor com argumentos
        Erro erro = new Erro("500", "Internal Server Error");

        // Verificar os valores dos atributos
        assertEquals("500", erro.getCodigo());
        assertEquals("Internal Server Error", erro.getMensagem());
    }

    @Test
    public void testDefaultConstructor() {
        // Criar uma instância da classe Erro usando o construtor padrão
        Erro erro = new Erro();

        // Verificar que os valores iniciais são nulos
        assertNull(erro.getCodigo());
        assertNull(erro.getMensagem());
    }

    @Test
    public void testSetCodigo() {
        // Criar uma instância da classe Erro
        Erro erro = new Erro();

        // Definir o código
        erro.setCodigo("401");

        // Verificar o código
        assertEquals("401", erro.getCodigo());
    }

    @Test
    public void testSetMensagem() {
        // Criar uma instância da classe Erro
        Erro erro = new Erro();

        // Definir a mensagem
        erro.setMensagem("Unauthorized");

        // Verificar a mensagem
        assertEquals("Unauthorized", erro.getMensagem());
    }
}
