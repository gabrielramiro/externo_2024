package com.bicicletario.externo.email;

import com.bicicletario.externo.model.Cobranca;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CobrancaTest {

    @Test
    public void testGettersAndSetters() {
        // Criar uma instância da classe Cobranca
        Cobranca cobranca = new Cobranca();

        // Definir valores usando os setters
        cobranca.setId(1);
        cobranca.setStatus("Pendente");
        cobranca.setHoraSolicitacao("10:00");
        cobranca.setHoraFinalizacao("12:00");
        cobranca.setValor(100.0);
        cobranca.setCiclista(123);

        // Verificar os valores usando os getters
        assertEquals(1, cobranca.getId());
        assertEquals("Pendente", cobranca.getStatus());
        assertEquals("10:00", cobranca.getHoraSolicitacao());
        assertEquals("12:00", cobranca.getHoraFinalizacao());
        assertEquals(100.0, cobranca.getValor());
        assertEquals(123, cobranca.getCiclista());
    }

    @Test
    public void testConstructorWithArgs() {
        // Criar uma instância da classe Cobranca usando o construtor com argumentos
        Cobranca cobranca = new Cobranca(1, "Pendente", "10:00", "12:00", 100.0, 123);

        // Verificar os valores dos atributos
        assertEquals(1, cobranca.getId());
        assertEquals("Pendente", cobranca.getStatus());
        assertEquals("10:00", cobranca.getHoraSolicitacao());
        assertEquals("12:00", cobranca.getHoraFinalizacao());
        assertEquals(100.0, cobranca.getValor());
        assertEquals(123, cobranca.getCiclista());
    }

    @Test
    public void testDefaultConstructor() {
        // Criar uma instância da classe Cobranca usando o construtor padrão
        Cobranca cobranca = new Cobranca();

        // Verificar que os valores iniciais são nulos ou zero
        assertNull(cobranca.getId());
        assertNull(cobranca.getStatus());
        assertNull(cobranca.getHoraSolicitacao());
        assertNull(cobranca.getHoraFinalizacao());
        assertNull(cobranca.getValor());
        assertNull(cobranca.getCiclista());
    }

    @Test
    public void testSetId() {
        // Criar uma instância da classe Cobranca
        Cobranca cobranca = new Cobranca();

        // Definir o id
        cobranca.setId(123);

        // Verificar o id
        assertEquals(123, cobranca.getId());
    }

    @Test
    public void testSetStatus() {
        // Criar uma instância da classe Cobranca
        Cobranca cobranca = new Cobranca();

        // Definir o status
        cobranca.setStatus("Concluído");

        // Verificar o status
        assertEquals("Concluído", cobranca.getStatus());
    }

    @Test
    public void testSetHoraSolicitacao() {
        // Criar uma instância da classe Cobranca
        Cobranca cobranca = new Cobranca();

        // Definir a hora de solicitação
        cobranca.setHoraSolicitacao("08:00");

        // Verificar a hora de solicitação
        assertEquals("08:00", cobranca.getHoraSolicitacao());
    }

    @Test
    public void testSetHoraFinalizacao() {
        // Criar uma instância da classe Cobranca
        Cobranca cobranca = new Cobranca();

        // Definir a hora de finalização
        cobranca.setHoraFinalizacao("09:00");

        // Verificar a hora de finalização
        assertEquals("09:00", cobranca.getHoraFinalizacao());
    }

    @Test
    public void testSetValor() {
        // Criar uma instância da classe Cobranca
        Cobranca cobranca = new Cobranca();

        // Definir o valor
        cobranca.setValor(200.0);

        // Verificar o valor
        assertEquals(200.0, cobranca.getValor());
    }

    @Test
    public void testSetCiclista() {
        // Criar uma instância da classe Cobranca
        Cobranca cobranca = new Cobranca();

        // Definir o ciclista
        cobranca.setCiclista(456);

        // Verificar o ciclista
        assertEquals(456, cobranca.getCiclista());
    }
}
