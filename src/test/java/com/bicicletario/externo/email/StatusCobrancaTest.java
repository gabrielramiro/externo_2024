package com.bicicletario.externo.email;

import com.bicicletario.externo.model.StatusCobranca;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class StatusCobrancaTest {

    @Test
    public void testValues() {
        // Verificar se todos os valores esperados estão presentes na enum
        StatusCobranca[] expectedValues = StatusCobranca.values();
        assertArrayEquals(new StatusCobranca[]{
                StatusCobranca.PENDENTE,
                StatusCobranca.PAGA,
                StatusCobranca.FALHA,
                StatusCobranca.CANCELADA,
                StatusCobranca.OCUPADA
        }, expectedValues);
    }

    @Test
    public void testValueOf() {
        // Verificar se o método valueOf retorna o valor correto
        assertEquals(StatusCobranca.PENDENTE, StatusCobranca.valueOf("PENDENTE"));
        assertEquals(StatusCobranca.PAGA, StatusCobranca.valueOf("PAGA"));
        assertEquals(StatusCobranca.FALHA, StatusCobranca.valueOf("FALHA"));
        assertEquals(StatusCobranca.CANCELADA, StatusCobranca.valueOf("CANCELADA"));
        assertEquals(StatusCobranca.OCUPADA, StatusCobranca.valueOf("OCUPADA"));
    }

    @Test
    public void testInvalidValueOf() {
        // Verificar se o método valueOf lança exceção para um valor inválido
        assertThrows(IllegalArgumentException.class, () -> {
            StatusCobranca.valueOf("INVALIDO");
        });
    }

    @Test
    public void testEnumNames() {
        // Verificar os nomes dos valores da enum
        assertEquals("PENDENTE", StatusCobranca.PENDENTE.name());
        assertEquals("PAGA", StatusCobranca.PAGA.name());
        assertEquals("FALHA", StatusCobranca.FALHA.name());
        assertEquals("CANCELADA", StatusCobranca.CANCELADA.name());
        assertEquals("OCUPADA", StatusCobranca.OCUPADA.name());
    }

    @Test
    public void testEnumOrdinal() {
        // Verificar a posição ordinal dos valores da enum
        assertEquals(0, StatusCobranca.PENDENTE.ordinal());
        assertEquals(1, StatusCobranca.PAGA.ordinal());
        assertEquals(2, StatusCobranca.FALHA.ordinal());
        assertEquals(3, StatusCobranca.CANCELADA.ordinal());
        assertEquals(4, StatusCobranca.OCUPADA.ordinal());
    }
}
