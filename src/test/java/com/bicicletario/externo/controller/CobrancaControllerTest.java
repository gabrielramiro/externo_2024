package com.bicicletario.externo.controller;

import com.bicicletario.externo.DTO.CobrancaDTO;
import com.bicicletario.externo.service.CobrancaService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

public class CobrancaControllerTest {

    @InjectMocks
    private CobrancaController cobrancaController;

    @Mock
    private CobrancaService cobrancaService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testRealizaCobrancaSuccess() {
        // Given
        CobrancaDTO cobrancaDTO = new CobrancaDTO();
        cobrancaDTO.setValor(100.0);
        cobrancaDTO.setCiclista(1);

        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("codigo", null);

        when(cobrancaService.incluirCobrancaNaFila(any(Double.class), any(Integer.class))).thenReturn(responseMap);

        // When
        ResponseEntity<Object> response = cobrancaController.realizaCobranca(cobrancaDTO);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(responseMap, response.getBody());
    }

    @Test
    public void testRealizaCobrancaError() {
        // Given
        CobrancaDTO cobrancaDTO = new CobrancaDTO();
        cobrancaDTO.setValor(100.0);
        cobrancaDTO.setCiclista(1);

        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("codigo", "400");
        responseMap.put("mensagem", "Erro de Teste");

        when(cobrancaService.incluirCobrancaNaFila(any(Double.class), any(Integer.class))).thenReturn(responseMap);

        // When
        ResponseEntity<Object> response = cobrancaController.realizaCobranca(cobrancaDTO);

        // Then
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(responseMap, response.getBody());
    }

    @Test
    public void testGetCobrancaSuccess() {
        // Given
        Integer idCobranca = 1;
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("codigo", null);

        when(cobrancaService.getCobranca(anyInt())).thenReturn(responseMap);

        // When
        ResponseEntity<Object> response = cobrancaController.getCobranca(idCobranca);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(responseMap, response.getBody());
    }

    @Test
    public void testGetCobrancaError() {
        // Given
        Integer idCobranca = 1;
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("codigo", "404");
        responseMap.put("mensagem", "Cobranca não encontrada");

        when(cobrancaService.getCobranca(anyInt())).thenReturn(responseMap);

        // When
        ResponseEntity<Object> response = cobrancaController.getCobranca(idCobranca);

        // Then
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
        assertEquals(responseMap, response.getBody());
    }
}
