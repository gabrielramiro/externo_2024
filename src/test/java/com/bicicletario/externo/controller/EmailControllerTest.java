package com.bicicletario.externo.controller;

import com.bicicletario.externo.DTO.EmailDTO;
import com.bicicletario.externo.service.EmailService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

public class EmailControllerTest {

    @InjectMocks
    private EmailController emailController;

    @Mock
    private EmailService emailService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testEnviaEmailSuccess() {
        // Given
        EmailDTO emailDTO = new EmailDTO();
        emailDTO.setEmail("test@example.com");
        emailDTO.setAssunto("Assunto de Teste");
        emailDTO.setMensagem("Mensagem de Teste");

        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("codigo", null);

        when(emailService.enviaEmail(anyString(), anyString(), anyString())).thenReturn(responseMap);

        // When
        ResponseEntity<Object> response = emailController.enviaEmail(emailDTO);

        // Then
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(responseMap, response.getBody());
    }

    @Test
    public void testEnviaEmailError() {
        // Given
        EmailDTO emailDTO = new EmailDTO();
        emailDTO.setEmail("test@example.com");
        emailDTO.setAssunto("Assunto de Teste");
        emailDTO.setMensagem("Mensagem de Teste");

        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("codigo", "400");
        responseMap.put("mensagem", "Erro de Teste");

        when(emailService.enviaEmail(anyString(), anyString(), anyString())).thenReturn(responseMap);

        // When
        ResponseEntity<Object> response = emailController.enviaEmail(emailDTO);

        // Then
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(responseMap, response.getBody());
    }

    @Test
    public void testEnviaEmailWithDifferentStatusCode() {
        // Given
        EmailDTO emailDTO = new EmailDTO();
        emailDTO.setEmail("test@example.com");
        emailDTO.setAssunto("Assunto de Teste");
        emailDTO.setMensagem("Mensagem de Teste");

        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("codigo", "500");
        responseMap.put("mensagem", "Erro Interno");

        when(emailService.enviaEmail(anyString(), anyString(), anyString())).thenReturn(responseMap);

        // When
        ResponseEntity<Object> response = emailController.enviaEmail(emailDTO);

        // Then
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, response.getStatusCode());
        assertEquals(responseMap, response.getBody());
    }
}
