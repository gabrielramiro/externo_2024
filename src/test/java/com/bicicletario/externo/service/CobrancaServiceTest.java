package com.bicicletario.externo.service;

import com.bicicletario.externo.model.Cobranca;
import com.bicicletario.externo.model.Erro;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import static com.bicicletario.externo.model.StatusCobranca.PENDENTE;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class CobrancaServiceTest {

    @InjectMocks
    private CobrancaService cobrancaService;

    @Mock
    private Cobranca cobranca;

    @Mock
    private Erro erro;

    @Mock
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        cobrancaService.setFilaCobrancas(new LinkedList<>()); // Initialize the queue
    }

    @Test
    public void testIncluirCobrancaNaFilaSuccess() {
        // Given
        Double valor = 100.0;
        Integer ciclista = 1;

        // Mocking the behavior of ObjectMapper
        when(objectMapper.convertValue(cobranca, new TypeReference<Map<String, Object>>() {}))
                .thenReturn(Map.of(
                        "id", 1,
                        "status", PENDENTE.name(),
                        "horaSolicitacao", "2024-07-24T22:19:54.525Z",
                        "horaFinalizacao", "2024-07-24T22:19:54.525Z",
                        "valor", valor,
                        "ciclista", ciclista
                ));

        // When
        Map<String, Object> response = cobrancaService.incluirCobrancaNaFila(valor, ciclista);

        // Then
        assertNotNull(response);
        assertEquals(1, response.get("id"));
        assertEquals(PENDENTE.name(), response.get("status"));
        assertEquals("2024-07-24T22:19:54.525Z", response.get("horaSolicitacao"));
        assertEquals("2024-07-24T22:19:54.525Z", response.get("horaFinalizacao"));
        assertEquals(valor, response.get("valor"));
        assertEquals(ciclista, response.get("ciclista"));
    }

    @Test
    public void testIncluirCobrancaNaFilaInvalidData() {
        // Given
        Double valor = -10.0; // Invalid value
        Integer ciclista = 1;

        // Mocking the behavior of ObjectMapper
        when(objectMapper.convertValue(any(Erro.class), new TypeReference<Map<String, Object>>() {}))
                .thenReturn(Map.of(
                        "codigo", "422",
                        "mensagem", "Dados Inválidos"
                ));

        // When
        Map<String, Object> response = cobrancaService.incluirCobrancaNaFila(valor, ciclista);

        // Then
        assertNotNull(response);
        assertEquals("422", response.get("codigo"));
        assertEquals("Dados Inválidos", response.get("mensagem"));
    }

    @Test
    public void testGetCobrancaFound() {
        // Given
        Integer id = 1;
        Cobranca cobranca = new Cobranca();
        cobranca.setId(id);
        cobranca.setStatus(PENDENTE.name());
        cobranca.setHoraSolicitacao("2024-07-24T22:19:54.525Z");
        cobranca.setHoraFinalizacao("2024-07-24T22:19:54.525Z");
        cobranca.setValor(100.0);
        cobranca.setCiclista(1);
        cobrancaService.getFilaCobrancas().add(cobranca);

        // Mocking the behavior of ObjectMapper
        when(objectMapper.convertValue(cobranca, new TypeReference<Map<String, Object>>() {}))
                .thenReturn(Map.of(
                        "id", id,
                        "status", PENDENTE.name(),
                        "horaSolicitacao", "2024-07-24T22:19:54.525Z",
                        "horaFinalizacao", "2024-07-24T22:19:54.525Z",
                        "valor", 100.0,
                        "ciclista", 1
                ));

        // When
        Map<String, Object> response = cobrancaService.getCobranca(id);

        // Then
        assertNotNull(response);
        assertEquals(id, response.get("id"));
        assertEquals(PENDENTE.name(), response.get("status"));
    }

    @Test
    public void testGetCobrancaNotFound() {
        // Given
        Integer id = 1;

        // Mocking the behavior of ObjectMapper
        when(objectMapper.convertValue(erro, new TypeReference<Map<String, Object>>() {}))
                .thenReturn(Map.of(
                        "codigo", "404",
                        "mensagem", "Cobrança não encontrada"
                ));

        // When
        Map<String, Object> response = cobrancaService.getCobranca(id);

        // Then
        assertNotNull(response);
        assertEquals("404", response.get("codigo"));
        assertEquals("Cobrança não encontrada", response.get("mensagem"));
    }
}
