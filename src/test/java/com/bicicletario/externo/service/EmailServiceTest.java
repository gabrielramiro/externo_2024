//package com.bicicletario.externo.service;
//import com.bicicletario.externo.model.Email;
//import com.bicicletario.externo.model.Erro;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//import org.springframework.mail.SimpleMailMessage;
//import org.springframework.mail.javamail.JavaMailSender;
//import org.springframework.web.client.RestTemplate;
//
//import java.util.HashMap;
//import java.util.Map;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.*;
//
//public class EmailServiceTest {
//
//    @Mock
//    private JavaMailSender mailSender;
//
//    @Mock
//    private Email email;
//
//    @Mock
//    private Erro erro;
//
//    @Mock
//    private RestTemplate restTemplate = new RestTemplate();
//
//    @InjectMocks
//    private EmailService emailService;
//
//    @BeforeEach
//    public void setUp() {
//        emailService = new EmailService();
//        emailService.mailSender = mailSender;
//        emailService.email = email;
//        emailService.erro = erro;
//    }
//
//    @Test
//    public void testEnviaEmailValid() throws Exception {
//        // Mockar a resposta da API de validação de email
////        emailService.email = "gabr";
////        when(emailService.ValidaEmail(anyString())).thenReturn("{\"status\":\"valid\",\"sub_status\":\"\"}");
//
//        // Mockar a resposta da API de validação de email
//        String mockApiResponse = "{\"status\":\"invalid\",\"sub_status\":\"mailbox_not_found\"}";
//        when(restTemplate.getForObject("anyString", String.class)).thenReturn(mockApiResponse);
//
//
//        // Chamar o método que queremos testar
//        Map<String, Object> response = emailService.enviaEmail("test@example.com", "Assunto", "Mensagem");
//
//        // Verificar que o email foi enviado
//        verify(mailSender, times(1)).send(any(SimpleMailMessage.class));
//
//        // Verificar que o email foi corretamente configurado
//        assertNotNull(response);
//        assertEquals("test@example.com", response.get("email"));
//        assertEquals("Assunto", response.get("assunto"));
//        assertEquals("Mensagem", response.get("mensagem"));
//    }
//
//    @Test
//    public void testEnviaEmailInvalidSyntax() throws Exception {
//        // Mockar a resposta da API de validação de email
//        String mockApiResponse = "{\"status\":\"invalid\",\"sub_status\":\"failed_syntax_check\"}";
//        when(restTemplate.getForObject(anyString(), eq(String.class))).thenReturn(mockApiResponse);
//
//        // Chamar o método que queremos testar
//        Map<String, Object> response = emailService.enviaEmail("invalid-email", "Assunto", "Mensagem");
//
//        // Verificar que o email não foi enviado
//        verify(mailSender, times(0)).send(any(SimpleMailMessage.class));
//
//        // Verificar que o erro foi corretamente configurado
//        assertNotNull(response);
//        assertEquals("422", response.get("codigo"));
//        assertEquals("failed_syntax_check", response.get("mensagem"));
//    }
//
//    @Test
//    public void testEnviaEmailMailboxNotFound() throws Exception {
//        // Mockar a resposta da API de validação de email
//        String mockApiResponse = "{\"status\":\"invalid\",\"sub_status\":\"mailbox_not_found\"}";
//        when(restTemplate.getForObject(anyString(), eq(String.class))).thenReturn(mockApiResponse);
//
//        // Chamar o método que queremos testar
//        Map<String, Object> response = emailService.enviaEmail("nonexistent@example.com", "Assunto", "Mensagem");
//
//        // Verificar que o email não foi enviado
//        verify(mailSender, times(0)).send(any(SimpleMailMessage.class));
//
//        // Verificar que o erro foi corretamente configurado
//        assertNotNull(response);
//        assertEquals("404", response.get("codigo"));
//        assertEquals("mailbox_not_found", response.get("mensagem"));
//    }
//}